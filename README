CAVE (Character Animation Viewer for Everyone) is an ascii animation viewer.
It uses (n)curses for it's interface so you'll need to have those libraries
installed. It currently has the following features:

    - frame stepping
    - scrolling for frames larger than screen
    - fps adjustment and frame cueing
    - supports multiple files and file reloading
    - looping
    - play in reverse
    - frame marking
    - screensaver with file shuffle and timeout adjustment
    - fullscreen toggle
    - change foreground and background colors
    - comprehensive status bar
    - builtin help
    - supports gzipped compressed files
    - extended regular expressions for frame deliminators
    - support for per-frame delays
    - autodetect position of frame deliminator

To install you should be able to type './configure && make && make install' to
configure, build and install the binary and manual page. Then type 'cave' to
see the available command-line options. If you have problems with the build,
let me know what OS your running and the version of the (n)curses libraries
you have installed. CAVE has been known to build on FreeBSD, MacOS X and
Linux. It should be fairly portable though.

A list of things planned can be found in the TODO file. Also, bugs that are
known about can be found in the BUGS file. Please let me know of any new bugs
you find.

Ben Kibbey <bjk@luxsci.net>
http://bjk.sourceforge.net/cave/
