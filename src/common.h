/*
    Copyright (C) 2001-2002 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifdef HAVE_REGEX_H
#include <sys/types.h>
#include <regex.h>
#endif

#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#ifndef HAVE_CURSES_H
#ifdef HAVE_NCURSES_H
#include <ncurses.h>
#endif
#else
#include <curses.h>
#endif

#ifdef HAVE_PANEL_H
#include <panel.h>
#endif

#ifndef LINE_MAX
#ifdef _POSIX2_LINE_MAX
#define LINE_MAX	_POSIX2_LINE_MAX
#elif defined(_POSIX_LINE_MAX)
#define LINE_MAX	_POSIX_LINE_MAX
#else
#define LINE_MAX	2048
#endif
#endif

#define ESCAPE '\033'
#define BACKSPACE '\010'

#define COPYRIGHT "Copyright (C) 2001-2002 " PACKAGE_BUGREPORT "\nDistributed under the terms of the GNU General Public License version 2."

#define LOADING "Loading %s, please wait..."
#define DETECT "could not detect file type for %s"
#define NOTAFILE "%s is not a regular file"
#define VINFO ("[ " PACKAGE_STRING " ]")
#define ERROR "[ ERROR ]"
#define NOTICE "[ NOTICE ]"
#define INFO "[ FRAME INFORMATION ]"
#define HELP "[ HELP ]"
#define STEP "STEP"
#define PLAY "PLAY"
#define GOTO "Goto frame: "
#define CHGSSTIMEOUT "Seconds: "
#define SSTIMEOUT "Seconds: "
#define LOOP "LOOP"
#define ANYKEY "[ press any key to continue ]"
#define HELPTEXT "Press 'h' for help"
#define FATAL (message("[ FATAL ]", ANYKEY, "%s(%i): %s", __FILE__, \
	    __LINE__ - 1, strerror(errno)))

#define ARRCNT(x)	(sizeof(x) / sizeof(x[0]))
#define CALCWIDTH(str) (strlen(str) + 4)	/*
						 * 2 for box, 2 for padding 
						 */
#define CALCHEIGHT() (4)	/*
				 * 2 box, 2 padding, 1 prompt 
				 */
#define CALCPOSY(y) ((y > LINES - 1) ? 0 : LINES / 2 - y / 2)
#define CALCPOSX(x) (COLS / 2 - x / 2)
#define CENTERX(x, str) (x / 2 - strlen(str) / 2)

typedef struct frames {
    char *line;
    struct frames *next;
} FRAME;

typedef struct scenes {
    unsigned long frameid;
    unsigned long delay;
    unsigned long timepos;
    unsigned rows;
    FRAME *frame;
    struct scenes *next;
    struct scenes *prev;
} SCENE;

int message(const char *, const char *, const char *, ...);
void *Malloc(size_t);
void *Calloc(size_t num, size_t size);
char *sepfile(char *);

unsigned regexdelay, maxx, maxy;
unsigned long frames, delaylen;
regex_t regex;
WINDOW *statusw;
PANEL *statusp;

#ifdef WITH_DMALLOC
#include <dmalloc.h>
#endif
