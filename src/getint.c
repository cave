/*  CAVE (Character Animation Viewer for Everyone)
    Copyright (C) 2001-2002 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "common.h"

#define MAXINPUT 9

static void updateinput(WINDOW * win, const char str[], unsigned x)
{
    int n = 0;

    wmove(win, 1, x);

    while (n != MAXINPUT)
	mvwaddch(win, 1, x + n++, ' ');

    mvwaddstr(win, 1, x, str);
    return;
}

long getint(char *prompt, int min)
{
    WINDOW *win;
    PANEL *panel;
    char str[MAXINPUT];
    unsigned long n = 0;
    int i = 0, c = 0;
    int x = strlen(prompt) + 1 + sizeof(str) + 4;

    win = newwin(3, x, CALCPOSY(3), CALCPOSX(x));
    box(win, ACS_VLINE, ACS_HLINE);
    panel = new_panel(win);

    mvwprintw(win, 1, 2, "%s", prompt);

    nl();

    x = strlen(prompt) + 2;
    bzero(str, sizeof(str));

    while ((c = wgetch(win)) != ESCAPE && c != '\n') {
	updateinput(win, str, x);

	if (c == BACKSPACE) {
	    if (!i)
		continue;

	    str[--i] = 0;
	    updateinput(win, str, x);
	    continue;
	}

	if (isdigit(c) == 0) {
	    beep();
	    continue;
	}

	if (i < sizeof(str))
	    str[i++] = c;
	else
	    beep();
	updateinput(win, str, x);
    }

    nonl();
    delwin(win);
    del_panel(panel);

    if (c == ESCAPE || str[0] == 0)
	return -1;

    n = atol(str);

    if (min != -1 && n < min) {
	beep();
	getint(prompt, min);
    }

    return n;
}
