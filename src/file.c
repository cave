/*  CAVE (Character Animation Viewer for Everyone)
    Copyright (C) 2001-2002 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_ZLIB_H
#include <zlib.h>
#endif

#include "common.h"

static long framedelim(const char *line)
{
    if (regexec(&regex, line, 1, 0, 0) == 0) {
	if (regexdelay)
	    return atol(line);
	else
	    return 0;
    }

    return -1;
}

SCENE *readfile(char *filename)
{
#ifdef HAVE_LIBZ
    gzFile *fp;
#else
    FILE *fp;
#endif
    unsigned rows = 0;
    long n, last = -1;
    char *line;
    int septop = 0;
    SCENE *scene, *snode, *sprev;
    FRAME *fnode;

    line = Calloc(1, LINE_MAX);

#ifdef HAVE_LIBZ
    if ((fp = gzopen(filename, "rb")) == NULL) {
#else
    if ((fp = fopen(filename, "r")) == NULL) {
#endif
	free(line);
	return NULL;
    }

#ifdef HAVE_LIBZ
    if ((line = gzgets(fp, line, LINE_MAX)) == NULL) {
#else
    if ((line = fgets(line, LINE_MAX, fp)) == NULL) {
#endif
	free(line);
	return NULL;
    }

    line[strlen(line) - 1] = 0;
    septop = (framedelim(line) != -1) ? 1 : 0;

#ifdef HAVE_LIBZ
    gzrewind(fp);
#else
    rewind(fp);
#endif

    message(NULL, NULL, LOADING, sepfile(filename));

    scene = Calloc(1, sizeof(SCENE));
    snode = scene;
    snode->frame = Calloc(1, sizeof(FRAME));
    fnode = snode->frame;
    frames = delaylen = 0;

#ifdef HAVE_LIBZ
    while ((line = gzgets(fp, line, LINE_MAX)) != NULL) {
#else
    while ((line = fgets(line, LINE_MAX, fp)) != NULL) {
#endif
	int len = strlen(line) - 1;

	line[len] = 0;
	n = framedelim(line);

	if (n != -1) {
	    if (septop) {
		if (last == -1) {
		    last = n;
		    continue;
		}
	    }
	    else
		last = n;

	    if (regexdelay) {
		delaylen += last;
		snode->timepos = delaylen;
		snode->delay = last;
	    }

	    last = n;
	    rows = 0;
	    snode->frameid = ++frames;
	    fnode->next = NULL;
	    snode->next = Calloc(1, sizeof(SCENE));
	    sprev = snode;
	    snode = snode->next;
	    snode->prev = sprev;
	    snode->frame = Calloc(1, sizeof(FRAME));
	    fnode = snode->frame;

	    continue;
	}

	fnode->line = strdup(line);
	fnode->next = Calloc(1, sizeof(FRAME));
	fnode = fnode->next;
	snode->rows = ++rows;

	if (maxx < len)
	    maxx = len;

	if (maxy < rows)
	    maxy = rows;
    }

    if (septop) {
	if (regexdelay) {
	    delaylen += last;
	    snode->timepos = delaylen;
	    snode->delay = last;
	}

	last = n;
	rows = 0;
	snode->frameid = ++frames;
	fnode->next = NULL;
	snode->next = Calloc(1, sizeof(SCENE));
	sprev = snode;
	snode = snode->next;
	snode->prev = sprev;
	snode->frame = Calloc(1, sizeof(FRAME));
	fnode = snode->frame;
    }

    snode->next = scene;
    scene->prev = snode->prev;

#ifdef HAVE_LIBZ
    gzclose(fp);
#else
    fclose(fp);
#endif

    free(line);

    message(NULL, NULL, NULL);
    return scene;
}
