/*  CAVE (Character Animation Viewer for Everyone)
    Copyright (C) 2001-2002 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

typedef struct files {
    unsigned fileid;
    char *filename;
    struct files *next;
    struct files *prev;
} FILES;

short foreg, backg;
unsigned ssalarm, sstimeout, isfullscreen;
unsigned fps, ssmode, loops, stepping, total, cue, ssquit;
struct itimerval itv;

enum { ERR_RELOADFILE = 1, ERR_NEXTFILE, ERR_PREVFILE, ERR_QUIT, ERR_SHUFFLE };

long getint(char *, int);
void help(void);
void updatestatus(FILES *, SCENE *, int, int, int, int);
SCENE *readfile(char *);

