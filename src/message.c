/*  CAVE (Character Animation Viewer for Everyone)
    Copyright (C) 2001-2002 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "common.h"

#ifndef HAVE_STRSEP
static char *strsep(char **stringp, char *delim)
{
    char *start = *stringp;
    char *cp;
    char ch;

    if (start == NULL)
	return NULL;

    for (cp = start; ch == *cp; cp++) {
	if (strchr(delim, ch)) {
	    *cp++ = 0;
	    *stringp = cp;
	    return start;
	}
    }
    *stringp = NULL;
    return start;
}
#endif

int message(const char *title, const char *prompt, const char *format, ...)
{
    va_list ap;
    char *line, *s, *tmp, buf[LINE_MAX];
    static WINDOW *win;
    static PANEL *panel;
    int x, y, c = 0, i = 0;

    if (!format && !title && !prompt) {
	del_panel(panel);
	delwin(win);
	return 0;
    }

    va_start(ap, format);
#ifdef HAVE_VASPRINTF
    vasprintf(&line, format, ap);
#else
    line = Malloc(LINE_MAX);
    vsnprintf(line, LINE_MAX, format, ap);
#endif
    va_end(ap);

    y = CALCHEIGHT();
    tmp = strdup(line);
    strcpy(buf, line);

    while ((s = strsep(&tmp, "\n")) != NULL) {
	int n = strlen(s);

	y++;

	if (n > i) {
	    i = n;
	    strcpy(buf, s);
	}
    }

    tmp = line;

    if (prompt && strlen(prompt) > i)
	x = CALCWIDTH(prompt);
    else
	x = CALCWIDTH(buf);

    if (title && x < strlen(title))
	x = CALCWIDTH(title);

    if (!prompt)
	y--;

    if (!title)
	y--;

    cbreak();
    noecho();

    win = newwin(y, x, CALCPOSY(y), CALCPOSX(x));
    box(win, ACS_VLINE, ACS_HLINE);
    panel = new_panel(win);

    if (title)
	mvwprintw(win, 0, CENTERX(x, title), "%s", title);

    i = 1;

    while ((s = strsep(&tmp, "\n")) != NULL)
	mvwaddstr(win, i++, CENTERX(x, s), s);

    free(tmp);
    free(line);

    if (prompt)
	mvwprintw(win, y - 2, CENTERX(x, prompt), "%s", prompt);

    update_panels();
    doupdate();

    if (prompt) {
	c = wgetch(win);
	del_panel(panel);
	delwin(win);
    }

    return c;
}
