/*  CAVE (Character Animation Viewer for Everyone)
    Copyright (C) 2001-2002 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
const char *helptext[] = {
    "UP/j    - increase frame rate          o - toggle looping",
    "DOWN/k  - decrease frame rate          r - play in reverse",
    "LEFT/l  - previous frame/reverse cue   i - frame information",
    "RIGHT/; - next frame/forward cue       f - toggle fullscreen",
    "SPACE   - start/stop animation         g - goto frame",
    "                                     u/U - change fore/background color",
    "a/A     - scroll up/first row",
    "z/Z     - scroll down/last row         s - toggle screensaver",
    "L/:     - scroll left/right            S - toggle screensaver (shuffle)",
    "                                       t - change screensaver timeout",
    "n       - next file",
    "p       - previous file                Q - quit",
    "R       - reload current file          v - version information"
};
